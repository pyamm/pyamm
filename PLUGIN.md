= The pyamm's PLUGIN SYSTEM =
Introduction to the plugin creating with Pyamm.

== Level ==
Pyamm works on the following levels:
 1. server
 1. domain
 1. account
 1. resource

Every level is indipendent and can have a double view, both for details option as well as table summary.

== User permission ==
Each user can have the following permission:
 * system administration
 * domain administration
 * user administration

= Write your own =
Please REMEMBER: use yaml syntax!

== Basic structure ==
 . Filename: myplugin.yml
 . Start a dictionary from myplugin, like
 {{{
myplugin:
  info:
    version: ...
}}}

== Infos ==
Every plugin has an information stanza. Basically you can add whatever you want, but I'd recommend:
 * version: plugin's version
 * creation_date: date of first plugin creation
 * updated_date: last time that the plugin has be updated
 * author: author name
 * mail: author email
 * web: author or plugin or service website
 * license: license (please use a freee software one, GPL3+ is raccomanded)
 * description: plugin description, what it does, how and why.

== Level configuration ==
Each has its own configuration. Just start a stanza with a level name (server|domain|account|resource).

The level needs the following attributes:
 * objectclass: a list with the required LDAP objectclass
 * rnd: the ldap rnd
 * attributes: list and configuration of all LDAP attribute

For further details read after.

=== Attributes ===
By default the domain and account levels are bind to the account plugin.
Instead the resource is never bind to it, while the server level cannot be unbind.

If the plugin is bound to the account's one then all of its options are set-up as an additional class (with all of its attributes) to the main domain/account/server object.
When instead the level is unbound then all of its object will be indipent from the same level object of the account plugin, but they will be children of a server/domain/account.

==== Attribute definition ====
Inside of the attributes stanza, each attribute will be an index. Such index has to be the LDAP attribute name.

Here the available options:
 * label: human readable name
 * help: long description of the attribute
 * field: field type, could be:
  - String
  - Select: in this case, add also an '{options: [ -{'key': 'value'} - {'key2': 'val2'} ]}
  - 
 * type: int (it will force html5 to read integer values. Usefull with field type if using int keys)
 * default: default value (used for new object creation)
 * read_auth: minimum required authorization level for reading
 * write_auth: minimum required authorization level for writing
 * show_order: display order (lower first)
 * index: see in Table, and hide in form
 * display_table

Proposed:
 * hide_from_field: do not display in form

=== Table ===
If the plugin is bound to the account system, than the navigation link is created from the domain or account plugin.
Instead if the plugin is unbound than the index will be created from the rnd attribute.

Special table types:
 * index: will create a link to the selected object
