# -*- coding: utf-8 -*-
"""Interface for administering accounts.

SERVICE LEVEL:
    0- server: not required
    1- domain: check dns, enable domain
    2- user: accounts and aliases
    3- connector: pull and push parameters
"""
# http://flask.pocoo.org/docs/0.12/views/
from flask import Blueprint, render_template, flash, url_for, redirect, current_app, request #pylint: disable=unused-import
from flask_login import login_required
import pyamm.fancy.utils as utils
import forms #pylint: disable=relative-import

accounts = Blueprint('accounts', __name__, url_prefix='/account')  #pylint: disable=invalid-name


class Account(utils.Pyamm):
    """Email decorator"""
    path = 'accounts.main'
    module_layout = {
        'account': {
            'level_2': {
                'objectclass': ['pyAccount', 'pyVirtualAccount'],
                'rnd': 'pyAccount',
                'attributes': {
                    'pyAccount': {'label': 'Account',
                                  'field': 'Hidden'},
                    'userPassword': {'label': 'password',
                                     'field': 'Shapassword'},
                    'pyDomainAdmin': {'label': 'Domain administrator',
                                      'field': 'Boolean'},
                    'pySystemAdmin': {'label': 'System administrator',
                                      'field': 'Boolean'},
                    'pyUserLanguage': {'label': 'User language',
                                       'field': 'String'}}},
            'table_0': {
                'attributes': {
                    'pyDomain': {'order': 1,
                                 'field': 'index',
                                 'label': 'domain'}},
                'filter': '(objectClass=pyDomain)'},
            'table_1': {
                'attributes': {
                    'pyAccount': {'order': 1,
                                  'field': 'index',
                                  'label': 'username'},
                    'pyDomainAdmin': {'order': 10,
                                      'field': 'boolean',
                                      'label': 'Domain admin'},
                    'pySystemAdmin': {'order': 5,
                                      'field': 'boolean',
                                      'label': 'System admin'}},
                'filter': '(objectClass=pyAccount)'}}}

    def set_template(self, nav, user): #pylint: disable=no-self-use,unused-argument
        """Templateing"""
        return 'panel/email.html'

    def check_permission(self, nav, user):
        """If the user permissions are not valid or out of bounds then redirect
        to a valid resource.
        """
        if user.level == 1 and nav.path[0] != user.domain:
            url = url_for(self.path, action=nav.action, domain=user.domain)
            return redirect(url, 302)
        elif user.level == 2 and (nav.path[0] != user.domain or nav.path[1] != user.username):
            url = url_for(self.path, action=nav.action,
                          domain=user.domain, account=user.username)
            return redirect(url, 302)
        # check module limits
        if nav.action == 'domain':
            if nav.path[1]:
                return redirect(url_for(self.path, action=nav.action, domain=nav.path[0]), 302)
        return True

    def set_options(self, nav, user):
        """Display object options"""
        if nav.action == 'account' and nav.level == 2:
            ldap = self.ldap_object(nav)
            if ldap:
                entry = ldap.get()
            else:
                entry = None
            if user.level == 2:
                form = forms.account_form(nav)
            elif user.level == 1:
                form = forms.account_form_domain(nav)
            elif user.level == 0:
                form = forms.account_form_sys(nav)
        else:
            return None
        if entry:
            return form(obj=entry)
        else:
            return form()

    def set_new(self, nav, user):
        return forms.account_add(nav)


view = login_required(Account.as_view('main')) #pylint: disable=invalid-name
accounts.add_url_rule('/', view_func=view)
accounts.add_url_rule('/<action>/', view_func=view)
accounts.add_url_rule('/<action>/<domain>/', view_func=view)
accounts.add_url_rule('/<action>/<domain>/<account>/', view_func=view)
accounts.add_url_rule('/<action>/<domain>/<account>/<resource>/', view_func=view)



