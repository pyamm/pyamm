# -*- coding: utf-8 -*-
"""Utility for fancy user interface.

- BaseForm: create WTForm
- Navigation: information about navigation (dn, level...)
- Ldap: interface to ldap and ldap abstraction layer
- Table: search and create table view
- UserInfo: current user details
- get_ldap: return an ldap proxy
- menu: to be implemented
"""

import os
import hashlib
import re
from base64 import encodestring as encode
import yaml
from flask import g, url_for, flash, current_app, redirect, render_template
from flask.views import View
import ldap3
from ldap3 import ObjectDef #AttrDef, ALL, Server, Connection
from ldap3.core.exceptions import LDAPNoSuchObjectResult, LDAPInsufficientAccessRightsResult
#    LDAPInvalidCredentialsResult, LDAPAttributeOrValueExistsResult
from flask_login import current_user
#FORMS
from flask_wtf import Form, html5
import wtforms
from wtforms.widgets import PasswordInput
from wtforms import validators
from wtforms_components import read_only
from pyamm.user import User


def get_ldap():
    """Return a ldap connection with superuser rights"""
    ldap_db = getattr(g, '_ldap', None)
    if ldap_db is None:
        user = 'cn=admin,dc=example,dc=tld'
        psw = current_app.config['ROOTLDAPPASSWORD']
        server = ldap3.Server('localhost')
        ldap_db = g._ldap = ldap3.Connection(server, user, psw, auto_bind=True)
    return ldap_db

def loader(source_path):
    """Load plugins from yaml file"""
    path = source_path
    with open(path, 'r') as source:
        try:
            module = yaml.load(source)
        except yaml.YAMLError as exc:
            current_app.logger.error('Not possible to load {}: {}'.format(path, exc))
    return module

def menu(nav, user):
    """Create structure for sidemenu"""
    infos = dict()
    infos['auth_level'] = user.level
    infos['module'] = nav.action
    return infos

class Navigation(object):
    """Abstraction of breadcrumb navigation"""
    levels_name = ('system', 'domain', 'account', 'resource')
    top_dn = 'o=pyamm,dc=example,dc=tld'

    def __init__(self, function, domain='', account='', resource='', action='', layout=''): #pylint: disable=too-many-arguments
        """Utility for navigation.

        Arguments (empy string if None):
         - function: main function name (required in self linking with url_for)
         - domain
         - account
         - resource
         - action
         - layout
        Special attributes:
         - navigation: a list of all attributes
        """
        self.path = (domain, account, resource)
        self.navigation = self.get_navigation()
        self.layout = layout
        self.level = len(self.navigation) - 1
        self.action = action
        self.function = function

    def rendering(self):
        """Return a dictionary with object usefull for the template rendering
        like:
        - level
        - sublevel
        """
        data = dict()
        data['level'] = self.level
        data['levelname'] = self.levels_name[self.level]
        if self.level < 3:
            data['sublevel'] = self.level + 1
            data['sublevelname'] = self.levels_name[self.level + 1]
        else:
            data['sublevel'] = data['sublevelname'] = False
        data['navigation'] = self.navigation
        data['action'] = self.action
        data['breadcrumb'] = self.breadcrumb()
        data['objectname'] = self.current_obj()
        data['form_url'] = self.form_url()
        data['object_url'] = self.object_url()
        return data

    def breadcrumb(self):
        """Return the breadcrumb elements in the form:
        [('obj_name', 'url'), (...), ...]
        """
        bread = list()
        for i in range(0, self.level+1):
            name = self.navigation[i]
            if i == 0:
                url = url_for(self.function, action=self.action)
            elif i == 1:
                url = url_for(self.function, action=self.action, domain=name)
            elif i == 2:
                url = url_for(self.function, action=self.action, domain=self.path[0], account=name)
            elif i == 3:
                url = url_for(self.function, action=self.action, domain=self.path[0],
                              account=self.path[1], resource=name)
            bread.append((name, url))
        return bread

    def current_obj(self):
        """Return the object on which the app is focused"""
        return self.navigation[-1]

    def get_navigation(self):
        """Create a list with the navigation path (stop adding element when an
        onject is empy)"""
        navigation = [self.levels_name[0]]
        for item in self.path:
            if item == '':
                return navigation
            else:
                navigation.append(item)
        return navigation

    def dn(self):
        """Return the object supposed dn.
        """
        key = self.level
        if isinstance(self.layout, dict):
            options = self.layout.get('level_{}'.format(key), dict()) #pylint: disable=no-member
            rnd = options.get('rnd', False)
        else:
            rnd = False
        _dn = None
        if key == 0:
            _dn = self.top_dn
        if rnd:
            if key == 1:
                _dn = '{}={},{}'.format(rnd, self.path[0], self.top_dn)
            elif key == 2:
                _dn = '{}={},pyDomain={},{}'.format(rnd, self.path[1],
                                                    self.path[0], self.top_dn)
            else:
                _dn = '{}={},pyAccount={},pyDomain={},{}'.format(rnd, self.path[2],
                                                                 self.path[1],
                                                                 self.path[0],
                                                                 self.top_dn)
        else:
            if key == 1:
                _dn = 'pyDomain={},{}'.format(self.path[0], self.top_dn)
            elif key == 2:
                _dn = 'pyAccount={},pyDomain={},{}'.format(self.path[1],
                                                           self.path[0],
                                                           self.top_dn)
        return _dn

    def object_url(self):
        """Return a dict with the object elements as:
        {'domain': 'example.com', 'account': 'me@example.com'}
        """
        path = dict()
        for i in range(0, self.level):
            path[self.levels_name[i+1]] = self.path[i]
        return path

    def form_url(self):
        """Rendering of url for current view forms"""
        path = self.object_url()
        path['action'] = self.action
        return url_for(self.function, **path)

    def redirect_to_new(self, obj=None):
        """Redirect to subobject if obj is present, otherwise redirect to self.
        """
        path = {'action': self.action}
        for i in range(0, self.level):
            path[self.levels_name[i+1]] = self.path[i]
        if obj:
            path[self.levels_name[self.level+1]] = obj
        return url_for(self.function, **path)



def breadcrumb(nav, user):
    """Return the breadcrumb elements in the form:
    [('obj_name', 'url'), (...), ...]
    """
    bread = list()
    for i in range(user.level, nav.level+1):
        name = nav.navigation[i]
        if i == 0:
            url = url_for(nav.function, action=nav.action)
        elif i == 1:
            url = url_for(nav.function, action=nav.action, domain=name)
        elif i == 2:
            url = url_for(nav.function, action=nav.action, domain=nav.path[0], account=name)
        elif i == 3:
            url = url_for(nav.function, action=nav.action, domain=nav.path[0],
                          account=nav.path[1], resource=name)
        bread.append((name, url))
    return bread



class Table(object):
    """Generate a table from an ldap database according to the given
    attributes.

    layout = {'attributes': {'active': {'order': 1,
                                        'type': bool,
                                        'label': 'coolstuff'}},
              'filter': (objectClass=%s),
              'objectclass': ['pyAccount', 'VirtualMail']}
    """

    safe_types = ('index', 'string', 'boolean')

    def __init__(self, connection, layout, navigation):
        """The following are required arguments:

         - conn: an LDAP binded connection object
         - layout: the attributes' configuration of the current option view
         - nav: navigation infos and dn
        """
        self.conn = connection
        self.layout = layout
        self.nav = navigation
        self.table = {'head': list(), 'body': list()}

    def search_filter(self):
        """Return the search filter"""
        return self.layout.get('filter', None)

    def attributes(self):
        """Return an ordered (by weight) list of attributes.
        If there is no attribute it will return None"""
        _list = dict()
        _ordered = list()
        if self.layout is None:
            return list()
        for _attr in self.layout.get('attributes', dict()).keys():
            order = self.layout['attributes'][_attr].get('order', 100)
            _list.setdefault(order, list()).append(_attr)
        for key in sorted(_list.keys()):
            _ordered.extend(_list[key])
        return _ordered

    def search(self):
        """Perform the LDAP search"""
        try:
            attr = self.attributes()
            attr.append('objectClass')
            search_filter = self.search_filter()
            if attr is None or len(attr) == 0 or search_filter is None:
                current_app.logger.info('Missing configuration parameter for search table')
                return False
            self.conn.search(self.nav.dn(),
                             search_filter,
                             search_scope=ldap3.LEVEL,
                             attributes=attr)
        except LDAPNoSuchObjectResult:
            current_app.logger.info('{} not found'.format(self.nav.dn()))
            return False
        return True

    def _header(self):
        """Setup the table header."""
        for attr in self.attributes():
            name = self.layout['attributes'].get(attr, dict()).get('label', '')
            if name == '':
                self.table['head'].append(attr)
            else:
                self.table['head'].append(name)

    def objclass_active(self, record):
        """Check whether the record has all of the required objectClass attributes.
        (self, dict(dict())) => Bool

        record: is a single ldap record like
            {'dn': '...', 'attributes': {'objectclass': ['Class1', 'Class2']...}}
        """
        objclass = self.layout.get('objectclass', False)
        if objclass:
            record_class = record['attributes']['objectClass']
            for required in objclass:
                if not required in record_class:
                    return False
        return True

    def _line(self, result):
        """Create a line for the table's body"""
        line = list()
        attr_opts = self.layout.get('attributes', dict())
        is_active = self.objclass_active(result)
        for attr in self.attributes():
            value = result['attributes'].get(attr, None)
            field = attr_opts.get(attr, dict()).get('field')
            if is_active or field == 'index':
                if field in self.safe_types:
                    func = getattr(self, field)
                    line.append(func(value))
                else:
                    line.append('System error')
            else:
                line.append('<p>Plugin not activated</p>')
        return line

    def index(self, value):
        """Rendering of the index"""
        url = self.url_to(value)
        content = '<a href="{URL}">{VAL}</a>'
        return content.format(URL=url, VAL=value)
    @staticmethod
    def string(value):
        """Rendering of string value"""
        return '<p>{}</p>'.format(value)
    @staticmethod
    def boolean(value):
        """Rendering of boolean field"""
        if value is True:
            return '<button type="button" class="btn btn-success">True</button>'
        elif value is False:
            return '<button type="button" class="btn btn-danger">False</button>'
        else:
            return '<button type="button" class="btn btn-default">Unset</button>'

    def url_to(self, obj):
        """Return the url to the object"""
        level = self.nav.level
        action = self.nav.action
        if level == 0:
            return url_for(self.nav.function, domain=obj, action=action)
        elif level == 1:
            return url_for(self.nav.function, domain=self.nav.path[0], account=obj, action=action)
        elif level == 2:
            return url_for(self.nav.function, domain=self.nav.path[0],
                           account=self.nav.path[1], resource=obj, action=action)

    def _body(self):
        """Setup the table body"""
        results = self.conn.response
        for result in results:
            self.table['body'].append(self._line(result))
        self.table['body'].sort()
        return True

    def set_table(self):
        """Setup and return the table"""
        if self.layout is None:
            return None
        if self.search():
            self._header()
            self._body()
        current_app.logger.info(self.table)
        return self.table


class BaseForm(Form):
    """Base for interactivelly creating forms.

    Usage example:
      Form = BaseForm.create('form_name')
      Form.append_field('email', EmailField('Email', [DataRequired(), Email()]))
      Form.append_field('name', StringField('Name', [Optional(), Length(max=64)]))
      Form.append_field('surname', Form.setfield({'field': 'String', 'label': 'User surname'}))
      Form.append_field('submit',
                        SubmitField('Update profile',
                                    render_kw={"class": "btn btn-success"}))
      Form.noedit = ['email', 'name']
    """
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        self.readonly()

    @classmethod
    def append_field(cls, name, field):
        """Class method for adding fields.
        Usage:
          cls.append_field('fieldname', fieldtype)"""
        if field is None:
            pass
        else:
            setattr(cls, name, field)
        return cls

    @classmethod
    def multi_append_field(cls, name, field):
        """Class method for adding a multi value field.
        Usage:
          cls.multi_append_field('fieldname', fieldtype)"""
        if field is None:
            pass
        else:
            multifield = getattr(wtforms, 'FieldList')
            setattr(cls, name, multifield(field))
        return cls

    def readonly(self):
        """Create readonly fields from list of values.
        Readonly is an html attribute only for text type fields."""
        if hasattr(self, 'noedit'):
            for form in self.noedit:
                read_only(getattr(self, form))

    @classmethod
    def append_ro_field(cls, name, field):
        """Class method for adding fields.
        Usage:
          cls.append_ro_field('fieldname', fieldtype)"""
        if field is None:
            pass
        else:
            setattr(cls, name, field)
        #    cls.noedit.append(name)
        return cls

    @classmethod
    def create(cls, subclass_name):
        """Create a child class of the current one with name subclass_name.

        This way every form will be an indipendent class."""
        new_form = type(subclass_name, (cls,), dict())
        setattr(new_form, 'noedit', list())
        return new_form

    @staticmethod
    def setfield(datasource): #pylint: disable=too-many-locals,too-many-branches
        """Return a wtform field type.

        datasource if a dictionary with the following keys:
         * field: the fieldtype
         * label: pretty label to display
         * options: select field options in the form [{key: value}, {key1: val1}]
         * requirements: list of requirement and validation
         * type: values: [int]: force html integer read

        Acceptable fields: 'URL', 'Email', 'Tel', 'String', 'Password', 'Submit', 'TextArea',
        'File', 'Date', 'Select', 'Radio', 'Boolean', 'Hidden'
        """
        field_args = list()
        field_kargs = dict()
        _html5 = ('URL', 'Email', 'Tel')
        _base = ('String', 'Password', 'Submit', 'TextArea', 'File', 'Date', 'Select', 'Boolean', \
                 'Hidden', 'Radio')
        _field = datasource.get('field', None)
        _type = datasource.get('type', None)
        _label = datasource.get('label', None)
        _options = datasource.get('options', None)
        _req = datasource.get('requirements', None)
        _rawreq = datasource.get('rawrequirements', None)
        if _field in _html5:
            field = getattr(html5, '{}Field'.format(_field))
        elif _field in _base:
            field = getattr(wtforms, '{}Field'.format(_field))
        elif _field == 'Shapassword':
            field = getattr(wtforms, 'StringField')
            field_kargs['widget'] = PasswordInput(hide_value=False)
        else:
            return None
        if _field == 'Select' or _field == 'Radio':
            if _options is None:
                return None
        if _label:
            field_args.append(_label)
        if _options:
            _arg = list()
            for line in _options:
                for key, value in line.iteritems():
                    _arg.append((key, value))
            field_kargs['choices'] = _arg
        if _req:
            _validators = list()
            base_val = ('Optional', 'DataRequired')
            for req in _req:
                if req in base_val:
                    _validators.append(getattr(validators, req)())
            field_args.append(_validators)
        elif _rawreq:
            field_args.append(_rawreq)
        if _type == 'int':
            field_kargs['coerce'] = int
        return field(*field_args, **field_kargs)



class Ldap(object): #pylint: disable=too-many-instance-attributes
    """Interface to ldap database.

    Requirements:
     - navigation: navigation object with dn informations
     - layout: plugin layout with LDAP informations
     - wentry_setup: add here custom formula for further wentry elaboration

    Main functions:
     - write: write changes to ldap
     - get: return ldap parameters
    """
    top_dn = 'o=pyamm,dc=example,dc=tld' #Top level of pyamm

    class Base(object): #pylint: disable=too-few-public-methods
        """Generic class used for form population and elaboration"""
        pass

    def __init__(self, navigation, layout, wentry_setup=''):
        """Parameters:
        - nav: navigation object
        - layout: a dictionary with the ldap object information:
            - rnd
            - objectclass
            - attributes
        - wentry_setup: function for additional wentry setup

        - entry: readable ldap entry
        - wentry: writable ldap  entry
        - filtered_entry: abstraction with parameters modification
        - new: tell if the object is new
        """
        # inputs
        self.nav = navigation
        self.layout = layout
        self.custom_wentry_setup = wentry_setup
        # default elements
        self.entry = None
        self.wentry = None
        self.filtered_entry = self.Base()
        self.new = None
        self.conn = get_ldap()
        # object iniziation
        self.read() # will set both entry and new

    def list_attributes(self):
        """Return the list of the required attributes"""
        attrs = self.layout.get('attributes', dict()).keys()
        return attrs

    def object_def(self):
        """Return the objectClass def"""
        return self.layout.get('objectclass')

    def check_objectclass(self):
        """Controll if the entry has all of the required objectClass.
        If any objectClass is missing then it will be added.
        """
        if self.wentry and not self.new:
            wentry_objectclass = self.wentry.objectClass
            config_objectclass = self.object_def()
            for objectclass in config_objectclass:
                if objectclass not in wentry_objectclass:
                    self.wentry.objectClass += objectclass

    def read(self):
        """Try to read attributes from LDAP and assignes found entry to self.entry

        If the element is found return True, otherwise False.
        """
        obj_definition = ObjectDef(self.object_def(), self.conn)
        try:
            ldap_cursor = ldap3.Reader(self.conn,
                                       obj_definition,
                                       self.nav.dn())
            #                  sub_tree=False) #search only at dn level
            #                  self._filter.format(self.rnd))
            current_app.logger.info('Starting a search of {}'.format(ldap_cursor))
            ldap_cursor.search_object()
            if len(ldap_cursor) == 1:
                self.entry = ldap_cursor[0]
                current_app.logger.info('Found {}'.format(self.entry))
                self.new = False
                return True
        except LDAPNoSuchObjectResult:
            current_app.logger.info('Object {} does not exists'.format(self.nav.dn()))
            return False
        current_app.logger.info('Nothing found.')
        self.new = True
        return False

    def set_filtered_entry(self):
        """Read from self.entry and create self.filtered_entry
        """
        attributes = self.layout['attributes']
        for attribute in self.list_attributes():
            multiple_field = attributes.get(attribute).get('multiple', False)
            if multiple_field:
                if hasattr(self.entry, attribute):
                    attr = getattr(self.entry, attribute)
                    value = attr.value
                    if value is None:
                        setattr(self.filtered_entry, attribute, list(['']))
                    elif isinstance(value, list):
                        value.append('')
                        setattr(self.filtered_entry, attribute, value)
                    else:
                        setattr(self.filtered_entry, attribute, [value, ''])
                else:
                    setattr(self.filtered_entry, attribute, list(['']))
            else:
                if hasattr(self.entry, attribute):
                    attr = getattr(self.entry, attribute)
                    setattr(self.filtered_entry, attribute, attr.value)


    @staticmethod
    def sha_password(password):
        """Create a ssha password with LDAP format.
        """
        salt = os.urandom(4)
        _hash = hashlib.sha1(password.encode('utf-8'))
        _hash.update(salt)
        return "{SSHA}" + encode(_hash.digest() + salt)[:-1]

    def get(self):
        """Try to get clean data from ldap, if found it return it, otherwise return False"""
        if self.new:
            return False
        else:
            self.set_filtered_entry()
            return self.filtered_entry

    def copy_to_filtered_entry(self, changes):
        """Copy attributes' value from changes to filtered_entry.

        'changes' variable has to be an object with the following structure:
        | changes(object):
        |     myattribute = 'myvalue'
        |     otherattribute = 'othervalue'
        """
        for attr in self.list_attributes():
            if hasattr(changes, attr):
                value = getattr(changes, attr).data
                setattr(self.filtered_entry, attr, value)

    def create_wentry(self):
        """Create a w(ritable)entry.

        If already exists an object into ldap it will be derivated from it, else
        a new object will be created.

        Requirement: self.new and self.entry
        """
        if self.new is None:
            current_app.logger.error("self.new need to be set")
            return False
        elif self.new is False:
            self.wentry = self.entry.entry_writable()
        elif self.new is True:
            obj = ObjectDef(self.object_def(), self.conn)
            writer = ldap3.Writer(self.conn, obj)
            self.wentry = writer.new(self.nav.dn())

    def set_wentry_attribute(self, attribute):
        """Setup wentry attribute coping from filtered_entry.

        If attribute is missing in filtered_entry nothing is done.

        Special field cases:
        - multi: is a field of fields
        - Shapassword: special algoritm for calculating new value
        """
        attributes = self.layout['attributes']
        field = attributes[attribute].get('field', None)
        multi = attributes[attribute].get('multiple', False)
        if hasattr(self.filtered_entry, attribute):
            new_value = getattr(self.filtered_entry, attribute)
            if hasattr(self.wentry, attribute):
                old_value = getattr(self.wentry, attribute).value
            else:
                old_value = None
            if field == 'Shapassword':
                if new_value != old_value:
                    new_value = self.sha_password(new_value)
            if multi:
                while '' in new_value:
                    new_value.remove('')
            if multi and len(new_value) == 0:
                pass
                # do not add empy list to ldap!
            elif new_value != old_value:
                setattr(self.wentry, attribute, new_value)

    def modify_wentry(self):
        """Write from self.filtered_entry to self.wentry.
        The writing will be done with value sanitation and trasformation.

        Exit status:
          - False: missing self.wentry
          - True: no execution error
        """
        if not self.wentry:
            current_app.logger.info('Unable to read from self.wentry. Skipping writing')
            return False
        if self.custom_wentry_setup is not '':
            self.custom_wentry_setup(self)
        else:
            for attribute in self.list_attributes():
                self.set_wentry_attribute(attribute)
        return True

    def commit(self):
        """Commit changes of self.wentry to LDAP.
        If errors occur return False.
        """
        try:
            self.check_objectclass()
            current_app.logger.info('Creating new object: {}'.format(self.wentry))
            return self.wentry.entry_commit_changes()
        except LDAPInsufficientAccessRightsResult:
            flash("You aren't allowed to create new object here.", 'danger')
            err = 'Not allowed to create following object: {}'
            current_app.logger.info(err.format(self.wentry))
        return False

    def write(self, changes):
        """If it's a commit write to ldap, if it's new create new object.
        """
        # To Do: populate from default (important for new account creation)
        curr = self.nav.navigation[-1]
        if self.new:
            #populate from default
            pass
        else:
            self.set_filtered_entry()
        self.copy_to_filtered_entry(changes)
        if self.new:
            ok_msg = '{} has been created'.format(curr)
            no_msg = 'Error creating {}'.format(curr)
        else:
            ok_msg = 'Options of {} are updated'.format(curr)
            no_msg = 'Error modifying {}'.format(curr)
        self.create_wentry()
        self.modify_wentry()
        if self.commit():
            flash(ok_msg, 'success')
        else:
            flash(no_msg, 'danger')



class UserInfo(object):
    """Helper for user administration"""
    domain_attribute = 'pyDomain'
    account_attribute = 'pyAccount'
    base = 'o=pyamm,dc=example,dc=tld'
    def __init__(self):
        """Get current user infos"""
        self.userdata = current_user.data
        self.level = self.user_level()
        self.username = current_user.username
        self.dn = current_user.dn
        self.domain = self._domain()

    def user_level(self):
        """Return the user level:
         0: system admin
         1: domain admin
         2: regular user
        """
        if self.userdata.get('pySystemAdmin', False):
            return 0
        elif self.userdata.get('pyDomainAdmin', False):
            return 1
        else:
            return 2

    def _domain(self):
        """Extract the user domain from its full dn"""
        base = r'{0}={1},{2}=([^,]+),{3}'.format(self.account_attribute,
                                                 self.username,
                                                 self.domain_attribute,
                                                 self.base)
        search = re.search(base, self.dn)
        return search.group(1)

    def minimum_path(self):
        """Return the minimum attribute that the user is suppose to be able to read.
        """
        if self.level == 0:
            return None
        if self.level == 1:
            return (self.domain,)
        if self.level == 2:
            return (self.domain, self.username)



class Pyamm(View): #pylint: disable=too-few-public-methods
    """Class with helper tools for pyamm management.
    Please setup:
     - module_layout
     - path
     - check_permission
     - set_options
     - set_new
    """
    methods = ['GET', 'POST']
    module_layout = dict()
    path = ''
    def __init__(self):
        """Init the class"""
        pass

    def dispatch_request(self, action='account', domain='', account='', resource=''): #pylint: disable=arguments-differ
        """Called when routing"""
        layout = self._get_layout(action)
        nav = Navigation(self.path, domain, account, resource,
                         action, layout)
        user = UserInfo()
        checks = self.check_permission(nav, user)
        if checks is not True:
            return checks
        options = self.set_options(nav, user)
        new = self.set_new(nav, user)
        side_menu = menu(nav, user)
        table_view = self.table_suboption(nav, get_ldap())
        if new:
            if new.validate_on_submit():
                new_url = nav.redirect_to_new(new.obj.data)
                return redirect(new_url, 302)
        if options:
            if options.validate_on_submit():
                self.write_options(nav, options)
                new_url = nav.redirect_to_new()
                return redirect(new_url, 302)
        return render_template(self.set_template(nav, user),
                               options=options,
                               suboption=table_view,
                               new=new,
                               nav=nav.rendering(),
                               breadcrumb=breadcrumb(nav, user),
                               menu=side_menu)

    def set_template(self, nav, user):
        """Return the template path"""
        pass

    def set_new(self, nav, user):
        """Set object options parameters"""
        pass

    def set_options(self, nav, user):
        """Set object options parameters"""
        pass

    def check_permission(self, nav, user): #pylint:disable=unused-argument,no-self-use
        """Set permission"""
        return True

    def _get_layout(self, action):
        """Return the layout for action"""
        return self.module_layout.get(action)

    def write_options(self, navigation, options):
        """Take value from form 'options' and write to ldap"""
        ldap = self.ldap_object(navigation)
        if ldap:
            ldap.write(options)
            if navigation.level == 2:
                _dn = navigation.dn()
                User.update_user(_dn)

    def table_suboption(self, navigation, connection):
        """Rendering of subobtion into table"""
        module_action = self.module_layout.get(navigation.action, dict())
        layout = module_action.get('table_{}'.format(navigation.level), None)
        table = Table(connection, layout, navigation)
        return table.set_table()

    def ldap_object(self, navigation):
        """Create, if set up in general configuration, the LDAP object for the current view.
        """
        module = self._get_layout(navigation.action)
        if module:
            if module.get('level_{}'.format(navigation.level)):
                layout = module['level_{}'.format(navigation.level)]
                modifier = ''
                if layout.get('modifier'):
                    modifier = getattr(self, layout.get('modifier'))
                return Ldap(navigation, layout, modifier)
        return None
