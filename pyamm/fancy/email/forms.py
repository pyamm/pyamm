# -*- coding: utf-8 -*-
"""Interface for administering emails and connectors.

SERVICE LEVEL:
    0- server: not required
    1- domain: check dns, enable domain
    2- user: accounts and aliases
    3- connector: pull and push parameters
"""
#pylint: disable=unused-import
import re
from flask_babel import gettext as _
from flask_wtf import Form
from flask_wtf.html5 import URLField, EmailField, TelField
from wtforms import ValidationError, StringField, PasswordField, SubmitField, \
    TextAreaField, FileField, DateField, DateTimeField, SelectField, BooleanField, \
    IntegerField
from wtforms.validators import DataRequired, Length, EqualTo, Email, URL, \
        AnyOf, Optional
from wtforms.widgets import PasswordInput
from wtforms_components import read_only
from flask import render_template, flash, url_for, redirect, current_app, request
from flask_login import login_required, current_user
from ldap3 import ALL, SUBTREE, Connection, Server, ObjectDef, AttrDef, Reader

import pyamm.fancy.utils as utils


def email_account_form(nav):
    """Return the form"""
    form = None
    if nav.action == 'account':
        if nav.level == 2:
            form = utils.BaseForm.create('form_email_account')
            form.append_ro_field('pyAccount', EmailField('Email'))
            form.append_field('userPassword', utils.BaseForm.setfield({'field': 'Shapassword',
                                                                       'label': 'Password'}))
            form.append_field('VirtualMailActive', BooleanField('Enabled'))
            select_type = utils.BaseForm.setfield({'field': 'Select',
                                                   'label': _('Type of account'),
                                                   'options': [{'MAILBOX': 'Imap account'},
                                                               {'ALIAS': 'Alias'}]})
            form.append_field('MailType', select_type)
            form.multi_append_field('VirtualMaildrop', StringField(_('Email redirects')))
            form.append_field('submit',
                              SubmitField(_('Update email account'),
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyAccount')
    return form


def email_domain_form(nav):
    """Return the form"""
    form = None
    if nav.action == 'domain':
        if nav.level == 1:
            form = utils.BaseForm.create('form_domain_domain')
            form.append_ro_field('pyDomain', StringField())
            form.append_field('pyDomainMTAstatus',
                              BooleanField('Activate the domain for receiving emails'))
            form.append_field('pyDelete', BooleanField('Mark for deletion'))
            form.append_field('submit',
                              SubmitField(_('Update domain'),
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyDomain')
    return form


def email_add(nav):
    """Display object options"""
    if nav.action == 'account':
        if nav.level == 1:
            domain = nav.path[0]
            search_f = '(&(objectClass=pyAccount)(pyAccount={}))'
            form = utils.BaseForm.create('form_AddUser_{}'.format(domain))
            form.append_field('obj', StringField('new email',
                                                 [DataRequired(), Email(), CheckEmail(domain),
                                                  CheckUniq(search_f)]))
            form.append_field('submit',
                              SubmitField('Create new email',
                                          render_kw={"class": "btn btn-success"}))
            return form()
    elif nav.action == 'domain':
        if nav.level == 0:
            search_f = '(&(objectClass=pyDomain)(pyDomain={}))'
            form = utils.BaseForm.create('form_AddDomain')
            form.append_field('obj', StringField('new domain',
                                                 [DataRequired(), CheckUniq(search_f)]))
            form.append_field('submit',
                              SubmitField('Create new domain',
                                          render_kw={"class": "btn btn-success"}))
            return form()
    elif nav.action == 'connector':
        if nav.level == 2:
            form = utils.BaseForm.create('form_AddConnector')
            form.append_field('obj', StringField('new connector',
                                                 [DataRequired(), Email()]))
            form.append_field('submit',
                              SubmitField('Create new connector',
                                          render_kw={"class": "btn btn-success"}))
            return form()
    return None


def email_connector_form(nav):
    """Form for connector setup"""
    form = None
    if nav.action == 'connector':
        if nav.level == 3:
            form = utils.BaseForm.create('form_email_connector')
            form.append_ro_field('pyMailConnector', EmailField('Email'))
            form.append_field('pyConnInboundEnabled', BooleanField(_('Enable fetching emails')))
            # pyConnFetchFolders     ("INBOX",)
            form.append_field('pyConnFetchFolders', StringField(_('Folder to download')))
            # pyConnPullProtocol     SimpleIMAPSSLRetriever...
            opt_type = {'field': 'Select', 'label': _('Type of protocol connection'),
                        'options': [{'SimpleIMAPSSLRetriever': 'IMAP over SSL'},
                                    {'SimplePOP3SSLRetriever': 'POP3 over SSL'}]}
            form.append_field('pyConnPullProtocol', utils.BaseForm.setfield(opt_type))
            # pyConnPullURL          imap.gmail.com
            form.append_field('pyConnPullURL',
                              utils.BaseForm.setfield({'field': 'String',
                                                       'label': _('Remote server URL')}))
            # pyConnRemoteDelete
            # pyConnUserLogin
            form.append_field('pyConnUserLogin', StringField(_('User for login')))
            # pyConnUserPassword
            form.append_field('pyConnUserPassword',
                              StringField(_('Password for remote login'),
                                          widget=PasswordInput(hide_value=False)))
            form.append_field('pyConnOutboundEnabled', BooleanField(_('Enable sending emails')))
            # pyConnLocalOwner       me@risca.eu <- same as dn
            # pyConnSMTPAuth         user:pw     <- from pyConnUserLogin/Password
            # pyConnSMTPURL          [smtp.gmail.com]:587
            form.append_field('pyConnSMTPURL', StringField(_('SMTP url for sending emails')))
            form.append_field('submit',
                              SubmitField(_('Update connector'),
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyMailConnector')
    return form




class CheckEmail(object): #pylint: disable=too-few-public-methods
    """Check it the fild.data domain's part is the same as pointed"""
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def sanitize(field):
        """Check that there is:
        - only one @
        - no -
        - only alphaNUM
        """
        if field.data.count('@') != 1:
            raise ValidationError(_("The email address needs to have one and only one '@'"))
        if not re.match(r"^[A-Za-z0-9_\.@]*$", field.data):
            raise ValidationError(_("Only letters, digits and '.' or '_' are accepted"))

    def isdomainrelated(self, field):
        """Check if it's part of the domain"""
        if field.data.split('@')[-1] != self.domain:
            errmesg = _("The email has to be part of the domain @{}")
            raise ValidationError(errmesg.format(self.domain))

    def __call__(self, form, field):
        self.sanitize(field)
        self.isdomainrelated(field)

class CheckUniq(object):  #pylint: disable=too-few-public-methods
    """Verify that the object is uniq and does not already exists.
    Usage:
      CheckUniq('(Account={})')
    """
    top_dn = 'o=pyamm,dc=example,dc=tld'

    def __init__(self, search_filter):
        self.connection = utils.get_ldap()
        self.search_filter = search_filter

    def search(self, obj):
        """Perform an LDAP search against obj"""
        search_filter = self.search_filter.format(obj)
        self.connection.search(self.top_dn, search_filter)
        if len(self.connection.entries) == 0:
            return False
        else:
            return True

    def __call__(self, form, field):
        if self.search(field.data):
            raise ValidationError(_("{} already exists").format(field.data))
        else:
            pass
