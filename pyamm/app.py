# -*- coding: utf-8 -*-
"""Istance for creating the app"""

from flask import Flask, render_template, session, flash, current_app, g, request
from flask_babel import gettext as _

from pyamm.config import DefaultConfig
from pyamm.user import User, USER

from pyamm.extensions import login_manager, ldap, babel
from pyamm.filters import format_date, pretty_date, nl2br
from pyamm.utils import INSTANCE_FOLDER_PATH
from flask_login import current_user


# For import *
__all__ = ['create_app']


def create_app(config=None, app_name=None):
    """Create a Flask app."""

    if app_name is None:
        app_name = DefaultConfig.PROJECT

    app = Flask(app_name, instance_path=INSTANCE_FOLDER_PATH, instance_relative_config=True)
    configure_app(app, config)
    configure_hook(app)
    configure_blueprints(app)
    configure_extensions(app)
    configure_logging(app)
    configure_template_filters(app)
    configure_error_handlers(app)
    configure_cli(app)

    return app


def configure_app(app, config=None):
    """Different ways of configurations."""

    # http://flask.pocoo.org/docs/api/#configuration
    app.config.from_object(DefaultConfig)

    # http://flask.pocoo.org/docs/config/#instance-folders
    app.config.from_pyfile('production.cfg', silent=True)

    if config:
        app.config.from_object(config)

    app.config.from_pyfile('/etc/pyamm/config.py', silent=True)
    # Use instance folder instead of env variables to make deployment easier.
    #app.config.from_envvar('%s_APP_CONFIG' % DefaultConfig.PROJECT.upper(), silent=True)


def configure_extensions(app):
    """Setup extensions"""

    # flask-login
    login_manager.login_view = 'frontend.home'
    login_manager.refresh_view = 'frontend.reauth'
    login_manager.login_message = _('Please log in to access this page.')
    login_manager.login_mmessage_category = 'info'

    @login_manager.user_loader
    def load_user(_id):  #pylint: disable=unused-variable
        """This function will call back the current user for login_manager"""
        if _id in USER:
            return USER[_id]
        else:
            return None
    login_manager.setup_app(app)

    #LDAP_conn
    ldap.init_app(app)

    @ldap.save_user
    def save_user(dn, username, data, memberships):  #pylint: disable=unused-variable,unused-argument
        """Set how to save the user from the ldap manager"""
        user = User(dn, username, data)
        USER[dn] = user
        return user

    #Babel
#    app.config['BABEL_DEFAULT_LOCALE'] = 'en'
    babel.init_app(app)

    @babel.localeselector
    def get_locale(): #pylint: disable=unused-variable
        """Select the locale language"""
        # if a user is logged in, use the locale from the user settings
        lang = getattr(g, 'user_lang', None)
        if lang in ['it', 'en']:
            return lang
        # otherwise try to guess the language from the user accept
        # header the browser transmits.  We support de/fr/en in this
        # example.  The best match wins.
        return request.accept_languages.best_match(['it', 'en'])



def configure_blueprints(app):
    """Configure blueprints in views."""

    from pyamm.user import user
    from pyamm.frontend import frontend
    from pyamm.fancy.email import email
    from pyamm.fancy.account import accounts

    for _bp in [user, frontend, email, accounts]:
        app.register_blueprint(_bp)


def configure_template_filters(app):
    """Configure filters."""

    app.jinja_env.filters["pretty_date"] = pretty_date
    app.jinja_env.filters["format_date"] = format_date
    app.jinja_env.filters["nl2br"] = nl2br


def configure_logging(app):
    """Configure file(info) and email(error) logging."""

    log_formatter = """%(asctime)s - %(levelname)s: %(filename)s: %(module)s: %(funcName)s:
    %(message)s
    [in %(pathname)s:%(lineno)d]"""

    import logging
    import os
    from logging.handlers import SMTPHandler

    if app.debug or app.testing:
        # Skip debug and test mode. Just check standard output.
        del app.logger.handlers[:]
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(logging.Formatter(log_formatter))
        console_handler.setLevel(logging.DEBUG)
        app.logger.addHandler(console_handler)
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.DEBUG)
        log.addHandler(console_handler)
        log2 = logging.getLogger('ldap3login')
        log2.setLevel(logging.DEBUG)
        log2.addHandler(console_handler)
        return

    log_path = os.path.join(app.config['LOG_FOLDER'], 'info.log')
    info_file_handler = logging.handlers.RotatingFileHandler(log_path,
                                                             maxBytes=100000,
                                                             backupCount=10)
    info_file_handler.setLevel(logging.INFO)
    info_file_handler.setFormatter(logging.Formatter(log_formatter))

    app.logger.addHandler(info_file_handler)

    # Testing
    #app.logger.info("testing info.")
    #app.logger.warn("testing warn.")
    #app.logger.error("testing error.")

    #Email logging
    #mail_handler = SMTPHandler(app.config['MAIL_SERVER'],
    #                           app.config['MAIL_USERNAME'],
    #                           app.config['ADMINS'],
    #                           'Your Application Failed!',
    #                           (app.config['MAIL_USERNAME'],
    #                            app.config['MAIL_PASSWORD']))
    #mail_handler.setLevel(logging.ERROR)
    #mail_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s '
    #                                            '[in %(pathname)s:%(lineno)d]'))
    #app.logger.addHandler(mail_handler)


def configure_hook(app):
    """Application hook"""

    from ldap3.core.exceptions import LDAPBindError
    from flask_login import COOKIE_NAME
    def reset_session():
        """Reset a user session cleaning also the cookies"""
        if 'user_id' in session:
            session.pop('user_id')
        if 'ldapPW' in session:
            session.pop('ldapPW')
        if '_fresh' in session:
            session.pop('_fresh')
        cookie_name = app.config.get('REMEMBER_COOKIE_NAME', COOKIE_NAME)
        if cookie_name in request.cookies:
            session['remember'] = 'clear'

    def connect_ldap():   #pylint: disable=unused-variable
        """Create a ldap connection"""
        if 'ldapPW' is session and 'user_id' in session:
            try:
                _user = session['user_id']
                g.ldap_conn = ldap._make_connection(_user, session['ldapPW'], contextualise=True) #pylint: disable=protected-access
            except LDAPBindError:
                flash('Not able to connect to LDAP', 'danger')
                reset_session()
        else:
            current_app.logger.info('Reset of session: user_id missing')
            reset_session()

    def master_ldap_close():
        """Close the ldap connection for master if used"""
        ldap_db = getattr(g, '_ldap', None)
        if ldap_db is not None:
            ldap_db.unbind()

    def set_g_user_lang():
        """Set the user language as global variable"""
        if current_user.is_authenticated:
            lang = current_user.data.get('pyUserLanguage', None)
            if lang is not None:
                g.user_lang = lang

    @app.before_request
    def before_request():         #pylint: disable=unused-variable
        """As it says"""
        set_g_user_lang()
        #connect_ldap()

    @app.teardown_appcontext
    def after_request(exception):         #pylint: disable=unused-variable,unused-argument
        """Something to cleanup?"""
        master_ldap_close()

def configure_error_handlers(app):
    """For info: http://flask.pocoo.org/docs/latest/errorhandling/"""

    @app.errorhandler(404)
    def page_not_found(error):      #pylint: disable=unused-argument,unused-variable
        """Redirect for error 404"""
        return render_template("errors/404.html"), 404


def configure_cli(app):      #pylint: disable=unused-argument
    """Add here cli command for Flask"""
    pass

    # Example
#    @app.cli.command()
#    def initdb():
#        db.drop_all()
#        db.create_all()
