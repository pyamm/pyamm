= PyAmm =
This project is inspired by phamm.

It aims to obtaing an easy administration pannel written in python, but a faster and simplified interface (both for admin and user).

= Install =

== Env ==
It is raccomanded to use virtualenv as follows:
{{{
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
}}}

=== Required package ===
If on debian, it is raccomanded to have installed:
{{{
python-dev virtualenv libmysqlclient-dev build-essential libffi-dev
}}}

== LDAP ==
On Debian systems is required to have installed:
 * slapd
 * ldap-utils

For schemas and configurations please watch into example directory.

= Translation =
The translation is done throught flask-babel.

There are two way for translating depending if in jinja file or code.

== Jinja ==
{{{
{% trans %}foo{% endtrans %}

{% trans num %}
There is {{ num }} object.
{% pluralize %}
There are {{ num }} objects.
{% endtrans %}

{{ _('Hello World!') }}
}}}
== Code ==
{{{
from flask_babel import gettext as _

print _(Hi, this will be translated)
}}}

== Create new localization ==
To update the source:
{{{
pybabel extract -F babel.cfg -o messages.pot ./pyamm
pybabel init -i messages.pot -d translations -l de
pybabel compile -d translations
}}}

But if just updateing current localization, run:
{{{
pybabel update -i messages.pot -d translations
}}}

== Infos ==
 * [[https://pythonhosted.org/Flask-Babel/|flask-babel module]]
 * [[http://jinja.pocoo.org/docs/2.9/templates/#i18n-in-templates|Jinja2 l18n]]
 * [[https://phraseapp.com/blog/posts/python-localization-for-flask-applications/|Localization examples]]

= Run =
For testing purpuse, just do:
{{{
source venv/bin/activate
FLASK_APP=./wsgi.py FLASK_TEST=1 flask run --port 5000
}}}

= Configuration =
It's possible to override the configuration writing in /etc/pyamm/config.py

= Security =
The security should be check at two levels:
 * Flask user permission
 * LDAP user permission (every user has a different connection to ldap)
